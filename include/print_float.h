#pragma once

#define PRINT_FLOAT_DECIMAL_COUNT 4


/* At the moment it is not possible to use floats in printf with A-Core.
 * This is just a temporary function for printing floats for now, for easier debugging.
 * Note that this print doesn't round, but cuts after PRINT_FLOAT_DECIMAL_COUNT decimals.
*/
void print_float(float f);


