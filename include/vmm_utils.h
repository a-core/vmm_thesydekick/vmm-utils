#pragma once

#include <stdbool.h>

#include "a-core-utils.h"
#include "a-core.h"
#include "test_macros.h"

#define VMM_RESET_CONTROL (A_CORE_AXI4LVMM + 0x00)
#define SRAM_DIN_CONTROL_ADDR (A_CORE_AXI4LVMM + 0x04)
#define VMM_READY_SRAM_DOUT (A_CORE_AXI4LVMM + 0x08)
#define OUTPUT_ADC (A_CORE_AXI4LVMM + 0x0C)

#define VMM_MAT_IN_ROWS 36
#define VMM_MAT_IN_COLS 32
#define VMM_VEC_IN_DIM 36
#define VMM_VEC_OUT_DIM 32


// Define bit position for each control bit included in VMM AXI4L registers.
// If there is a sequence of bits, define position of first bit and a width
// of the sequence.

// VMM_RESET_CONTROL
#define VMM_ADC_MUX_WR 0
#define VMM_VCM_EN 1
#define VMM_SH_EN 2
#define VMM_RESET_ADC 3
#define VMM_RESET_SRAM 4
#define VMM_RESET_OP_COMP 5
#define VMM_RESET_IP_COMP 6
#define VMM_R3_TUNE 8
#define VMM_R3_TUNE_WIDTH 9
#define VMM_LSB_TUNE 17
#define VMM_COMP_RDB_WR_GBL 18
#define VMM_COMP_EN 19
#define VMM_R_TUNE 20
#define VMM_ADC_MUX_CONFIG 24
#define VMM_ADC_MUX_CONFIG_WIDTH 5

// SRAM_DIN_CONTROL_ADDR
#define VMM_SRAM_ADDR 0
#define VMM_SRAM_ADDR_WIDTH 12
#define VMM_SRAM_WR 14
#define VMM_SRAM_RD 13
#define VMM_SRAM_DIN 16
#define VMM_SRAM_DIN_WIDTH 8

// VMM_READY_SRAM_DOUT
#define VMM_SRAM_DOUT 0
#define VMM_SRAM_DOUT_WIDTH 8
#define VMM_ADC_DATA_READY 8
#define VMM_HOLD_DATA_READY 9


/* ************************************************************************************* */
/* ************************************************************************************* */

/* row range: [0 ,35]
 * col range: [0, 31] */
void vmm_write_mat_in_bin(unsigned row, unsigned col, uint8_t val);

/* col range: [0, 35] */
void vmm_write_vec_in_bin(unsigned col, uint8_t val);

/* row range: [0 ,35]
 * col range: [0, 31] */
void vmm_write_mat_in_float(unsigned row, unsigned col, float val);

/* col range: [0, 35] */
void vmm_write_vec_in_float(unsigned col, float val);

/* col range: [0, 31] */
uint8_t vmm_read_vec_out_bin(unsigned col);

/* col range: [0, 31] */
float vmm_read_vec_out_float(unsigned col);


/* ************************************************************************************* */
/* Read / write AXI4L registers                                                          */
/* ************************************************************************************* */

/*
Content and default values for AXI4L registers:
 
VMM_RESET_CONTROL
    [XXX adc_mux_config<4:0>(5'b0 0000)]
    [XXX r_tune(1'b1) , comp_en(1'b0), comp_rdb_wr_gbl(1'b0), lsb_tune(1'b0), r3_tune<8>(1'b1)]
    [r3_tune<7:0>(8'b0000 0000)] 
    [X, reset_ip_comp(1'b0), reset_op_comp(1'b0), reset_sram(1'b0), reset_adc(1'b0), sh_en(1'b0), vcm_en(1'b0), adc_mux_wr(1'b0)]


SRAM_DIN_CONTROL_ADDR
    [XXXX XXXX] 
    [sram_din<7:0>(8'b0000 0000)] 
    [X, sram_wr(1'b0), sram_rd(1'b0), X, sram_addr<11:8>(4'b0000)] 
    [sram_addr<7:0>(8'b0000 0000)] 

VMM_READY_SRAM_DOUT:
    [XXXX XXXX] 
    [XXXX XXXX] 
    [XXXX XX hold_data_ready(1'b0) ,adc_data_ready(1'b0)] 
    [sram_dout<7:0>(8'b0000 0000)] 
*/

// **************************** //
// Register: VMM_RESET_CONTROL  //
// **************************** //

/* adc_mux_config (5 bits):
 * Controls the ADC counter that selects the ADC MUX.
 * Default case selects the first MUX. */
void vmm_write_adc_mux_config(uint32_t val);
uint32_t vmm_read_adc_mux_config();

/* r3_tune (9 bits):
 * Controls the switches in series with the sub-amp feedback resistors.
 * 9 bits for closed-loop gain control. Default: 1 Rfb is connected.
 * All need to be 0 during compensation. */
void vmm_write_r3_tune(uint32_t val);
uint32_t vmm_read_r3_tune();

/* r_tune
 * Controls the switches in series with the sum-amp feedback resistor and resisitor between
 * sum and sub amps. Default: Rfbs is connected. Needs to be 0 during compensation. */
void vmm_write_r_tune(bool val);
bool vmm_read_r_tune();

/* comp_en:
 * To do automatic compensation (active High) */
void vmm_write_comp_en(bool val);
bool vmm_read_comp_en();

/* comp_rdb_wr_gbl:
 * When 0 we monitor the compensated values. 1 we force the compensation value. */
void vmm_write_comp_rdb_wr_gbl(bool val);
bool vmm_read_comp_rdb_wr_gbl();

/* lsb_tune:
 * This is for compensation to add 1 or 0. */
void vmm_write_lsb_tune(bool val);
bool vmm_read_lsb_tune();

/* reset_ip_comp:
 * Resets input compensation to 0s when 1 is given */
void vmm_write_reset_ip_comp(bool val);
bool vmm_read_reset_ip_comp();

/* reset_op_comp:
 * Resets output compensation to 0s when 1 is given */
void vmm_write_reset_op_comp(bool val);
bool vmm_read_reset_op_comp();

/* reset_sram:
 * Resets top control. Default is 0 (Not reseted). Reset is active high */
void vmm_write_reset_sram(bool val);
bool vmm_read_reset_sram();

/* reset_adc:
 * Resets the adc block. Default is 0 (Not reseted). Reset is active high. */
void vmm_write_reset_adc(bool val);
bool vmm_read_reset_adc();

/* sh_en:
 * Sample is 0 and hold 1 */
void vmm_write_sh_en(bool val);
bool vmm_read_sh_en();

/* vcm_en:
 * Must be 1 when we do compensation. Connects op-amps -ive terminal to vcm.
 * Enables compensation output buffer. */
void vmm_write_vcm_en(bool val);
bool vmm_read_vcm_en();

/* adc_mux_wr:
 * When 1 we can force the mux adc counter to control which column we can read. */
void vmm_write_adc_mux_wr(bool val);
bool vmm_read_adc_mux_wr();


// ******************************** //
// Register: SRAM_DIN_CONTROL_ADDR  //
// ******************************** //

/* sram_addr (12 bits):
 * The sram address in the top control address block. */
void vmm_write_sram_addr(uint32_t val);
uint32_t vmm_read_sram_addr();


/* sram_din (8 bits):
 * 8-bit bus which control the D-flops in the top control. Default value is all 0 */
void vmm_write_sram_din(uint32_t val);
uint32_t vmm_read_sram_din();

/* sram_wr:
 * Controls the write signal in the top control read write flop. Default is 0. */
void vmm_write_sram_wr(bool val);
bool vmm_read_sram_wr();

/* sram_rd:
 * Controls the read signal in the top control adderss flop. Default is 0. */
void vmm_write_sram_rd(bool val);
bool vmm_read_sram_rd();


// ******************************** //
// Register: VMM_READY_SRAM_DOUT    //
// ******************************** //

/* hold_data_ready:
 * When 1 means input is given to adc */
bool vmm_read_hold_data_ready();

/* adc_data_ready:
 * Output digital signal from adc counter. Default value is 0.
 * Value 1 indicates that adc conversion is compeleted. */
bool vmm_read_adc_data_ready();

/* sram_dout (8 bits):
 * To monitor the data written to the sram_din<7:0>. */
uint32_t vmm_read_sram_dout();


// ******************************** //
// Register: OUTPUT_ADC             //
// ******************************** //

/* adc_00 - adc_31 (6 bits):
 * Index 0 -> adc_00, index 1 -> adc_01 etc. Using index outside 0-31 results in undefined behavior.
 * Is the 6-bit digital output of the time-multiplixed flash adc. */
uint8_t vmm_read_adc(unsigned index);


/* ************************************************************************************* */
/* Input / output formatting                                                             */
/* ************************************************************************************* */

uint8_t vmm_float2bin_vec_in(float val);
uint8_t vmm_float2bin_mat_in(float val);
float vmm_bin2float_vec_out(uint8_t val);
void vmm_print_result(); // TODO: This is a temporary debugging function


