#ifdef ACORE

#include <stdint.h>
#include <stdio.h>
#include <math.h>
#include <sys/types.h>

#include "vmm_utils.h"
#include "a-core.h"
#include "print_float.h" // TODO: Get rid of this after printing floating point works


/* ************************************************************************************* */
/* ************************************************************************************* */

void vmm_write_mat_in_bin(unsigned row, unsigned col, uint8_t val) {
    const int SRAM_MAT_IN_START_ADDR = 128; // TODO Possibly make a define out of this?
    const int SRAM_MAT_IN_OFFSET = 64; // TODO Possibly make a define out of this?
    uint32_t word = (1 << VMM_SRAM_WR);
    word |= (SRAM_MAT_IN_START_ADDR + col*SRAM_MAT_IN_OFFSET + row) << VMM_SRAM_ADDR;
    word |= (val) << VMM_SRAM_DIN;
    gpo_write((volatile uint32_t *)SRAM_DIN_CONTROL_ADDR, word);
}

void vmm_write_vec_in_bin(unsigned col, uint8_t val) {
    const int SRAM_VEC_IN_START_ADDR = 0;
    uint32_t word = (1UL << VMM_SRAM_WR);
    word |= (SRAM_VEC_IN_START_ADDR + col) << VMM_SRAM_ADDR;
    word |= (val) << VMM_SRAM_DIN;
    gpo_write((volatile uint32_t *)SRAM_DIN_CONTROL_ADDR, word);
}

void vmm_write_mat_in_float(unsigned row, unsigned col, float val) {
    vmm_write_mat_in_bin(row, col, vmm_float2bin_mat_in(val));
}

void vmm_write_vec_in_float(unsigned col, float val) {
    vmm_write_vec_in_bin(col, vmm_float2bin_vec_in(val));
}

uint8_t vmm_read_vec_out_bin(unsigned col) {
    return vmm_read_adc(col);
}

float vmm_read_vec_out_float(unsigned col) {
    return vmm_bin2float_vec_out(vmm_read_adc(col));
}


/* ************************************************************************************* */
/* Read / write AXI4L registers                                                          */
/* ************************************************************************************* */

static inline void bit_write(volatile uint32_t *addr, uint32_t bit_mask, bool val) {
    uint32_t word_old = gpi_read(addr);
    uint32_t word_new;
    if (val) {
        word_new = word_old | bit_mask;
    } else {
        word_new = word_old & (~bit_mask);
    }
    gpo_write((volatile uint32_t *)addr, word_new);
}

static inline bool bit_read(volatile uint32_t *addr, uint32_t bit_mask) {
    uint32_t ret = gpi_read(addr);
    return ret & bit_mask;
}

static inline void uint_write(volatile uint32_t *addr, uint32_t bit_mask, unsigned first_bit_index, uint32_t val) {
    uint32_t word_old = gpi_read(addr);
    uint32_t word_new;
    word_new = (val << first_bit_index) & bit_mask;
    word_new |= (word_old & (~bit_mask));
    gpo_write((volatile uint32_t *)addr, word_new);
}

static inline uint32_t uint_read(volatile uint32_t *addr, uint32_t bit_mask, unsigned first_bit_index) {
    uint32_t ret = gpi_read(addr);
    return (ret & bit_mask) >> first_bit_index;
}

// **************************** //
// Register: VMM_RESET_CONTROL  //
// **************************** //

// Write bit sequences

void vmm_write_adc_mux_config(uint32_t val) {
    uint_write(
        (volatile uint32_t *)VMM_RESET_CONTROL,
        (((1 << VMM_ADC_MUX_CONFIG_WIDTH) - 1) << VMM_ADC_MUX_CONFIG),
        VMM_ADC_MUX_CONFIG,
        val
    );
}

void vmm_write_r3_tune(uint32_t val) {
    uint_write(
        (volatile uint32_t *)VMM_RESET_CONTROL,
        (((1 << VMM_R3_TUNE_WIDTH) - 1) << VMM_R3_TUNE),
        VMM_R3_TUNE,
        val
    );
}

// Write single bits

void vmm_write_r_tune(bool val) {
    bit_write(
        (volatile uint32_t *)VMM_RESET_CONTROL,
        (1 << VMM_R_TUNE),
        val
    );
}

void vmm_write_comp_en(bool val) {
    bit_write(
        (volatile uint32_t *)VMM_RESET_CONTROL,
        (1 << VMM_COMP_EN),
        val
    );
}

void vmm_write_comp_rdb_wr_gbl(bool val) {
    bit_write(
        (volatile uint32_t *)VMM_RESET_CONTROL,
        (1 << VMM_COMP_RDB_WR_GBL),
        val
    );
}

void vmm_write_lsb_tune(bool val) {
    bit_write(
        (volatile uint32_t *)VMM_RESET_CONTROL,
        (1 << VMM_LSB_TUNE),
        val
    );
}

void vmm_write_reset_ip_comp(bool val) {
    bit_write(
        (volatile uint32_t *)VMM_RESET_CONTROL,
        (1 << VMM_RESET_IP_COMP),
        val
    );
}

void vmm_write_reset_op_comp(bool val) {
    bit_write(
        (volatile uint32_t *)VMM_RESET_CONTROL,
        (1 << VMM_RESET_OP_COMP),
        val
    );
}

void vmm_write_reset_sram(bool val) {
    bit_write(
        (volatile uint32_t *)VMM_RESET_CONTROL,
        (1 << VMM_RESET_SRAM),
        val
    );
}

void vmm_write_reset_adc(bool val) {
    bit_write(
        (volatile uint32_t *)VMM_RESET_CONTROL,
        (1 << VMM_RESET_ADC),
        val
    );
}

void vmm_write_sh_en(bool val) {
    bit_write(
        (volatile uint32_t *)VMM_RESET_CONTROL,
        (1 << VMM_SH_EN),
        val
    );
}

void vmm_write_vcm_en(bool val) {
    bit_write(
        (volatile uint32_t *)VMM_RESET_CONTROL,
        (1 << VMM_VCM_EN),
        val
    );
}

void vmm_write_adc_mux_wr(bool val) {
    bit_write(
        (volatile uint32_t *)VMM_RESET_CONTROL,
        (1 << VMM_ADC_MUX_WR),
        val
    );
}

// Read bit sequences

uint32_t vmm_read_adc_mux_config() {
    return uint_read(
        (volatile uint32_t *)VMM_RESET_CONTROL,
        (((1 << VMM_ADC_MUX_CONFIG_WIDTH) - 1) << VMM_ADC_MUX_CONFIG),
        VMM_ADC_MUX_CONFIG
    );
}

uint32_t vmm_read_r3_tune() {
    return uint_read(
        (volatile uint32_t *)VMM_RESET_CONTROL,
        (((1 << VMM_R3_TUNE_WIDTH) - 1) << VMM_R3_TUNE),
        VMM_R3_TUNE
    );
}

// Read single bits

bool vmm_read_r_tune() {
    return bit_read(
        (volatile uint32_t *)VMM_RESET_CONTROL,
        (1 << VMM_R_TUNE)
    );
}

bool vmm_read_comp_en() {
    return bit_read(
        (volatile uint32_t *)VMM_RESET_CONTROL,
        (1 << VMM_COMP_EN)
    );
}

bool vmm_read_comp_rdb_wr_gbl() {
    return bit_read(
        (volatile uint32_t *)VMM_RESET_CONTROL,
        (1 << VMM_COMP_RDB_WR_GBL)
    );
}

bool vmm_read_lsb_tune() {
    return bit_read(
        (volatile uint32_t *)VMM_RESET_CONTROL,
        (1 << VMM_LSB_TUNE)
    );
}

bool vmm_read_reset_ip_comp() {
    return bit_read(
        (volatile uint32_t *)VMM_RESET_CONTROL,
        (1 << VMM_RESET_IP_COMP)
    );
}

bool vmm_read_reset_op_comp() {
    return bit_read(
        (volatile uint32_t *)VMM_RESET_CONTROL,
        (1 << VMM_RESET_OP_COMP)
    );
}

bool vmm_read_reset_sram() {
    return bit_read(
        (volatile uint32_t *)VMM_RESET_CONTROL,
        (1 << VMM_RESET_SRAM)
    );
}

bool vmm_read_reset_adc() {
    return bit_read(
        (volatile uint32_t *)VMM_RESET_CONTROL,
        (1 << VMM_RESET_ADC)
    );
}

bool vmm_read_sh_en() {
    return bit_read(
        (volatile uint32_t *)VMM_RESET_CONTROL,
        (1 << VMM_SH_EN)
    );
}

bool vmm_read_vcm_en() {
    return bit_read(
        (volatile uint32_t *)VMM_RESET_CONTROL,
        (1 << VMM_VCM_EN)
    );
}

bool vmm_read_adc_mux_wr() {
    return bit_read(
        (volatile uint32_t *)VMM_RESET_CONTROL,
        (1 << VMM_ADC_MUX_WR)
    );
}


// ******************************** //
// Register: SRAM_DIN_CONTROL_ADDR  //
// ******************************** //

void vmm_write_sram_addr(uint32_t val) {
    uint_write(
        (volatile uint32_t *)SRAM_DIN_CONTROL_ADDR,
        (((1 << VMM_SRAM_ADDR_WIDTH) - 1) << VMM_SRAM_ADDR),
        VMM_SRAM_ADDR,
        val
    );
}

void vmm_write_sram_din(uint32_t val) {
    uint_write(
        (volatile uint32_t *)SRAM_DIN_CONTROL_ADDR,
        (((1 << VMM_SRAM_DIN_WIDTH) - 1) << VMM_SRAM_DIN),
        VMM_SRAM_DIN,
        val
    );
}

void vmm_write_sram_wr(bool val) {
    bit_write(
        (volatile uint32_t *)SRAM_DIN_CONTROL_ADDR,
        (1 << VMM_SRAM_WR),
        val
    );
}

void vmm_write_sram_rd(bool val) {
    bit_write(
        (volatile uint32_t *)SRAM_DIN_CONTROL_ADDR,
        (1 << VMM_SRAM_RD),
        val
    );
}


uint32_t vmm_read_sram_addr() {
    return uint_read(
        (volatile uint32_t *)SRAM_DIN_CONTROL_ADDR,
        ((( 2 << (VMM_SRAM_ADDR_WIDTH - 1)) - 1) << VMM_SRAM_ADDR),
        VMM_SRAM_ADDR
    );
}

uint32_t vmm_read_sram_din() {
    return uint_read(
        (volatile uint32_t *)SRAM_DIN_CONTROL_ADDR,
        (((1 << VMM_SRAM_DIN_WIDTH) - 1) << VMM_SRAM_DIN),
        VMM_SRAM_DIN
    );
}

bool vmm_read_sram_wr() {
    return bit_read(
        (volatile uint32_t *)SRAM_DIN_CONTROL_ADDR,
        (1 << VMM_SRAM_WR)
    );
}

bool vmm_read_sram_rd() {
    return bit_read(
        (volatile uint32_t *)SRAM_DIN_CONTROL_ADDR,
        (1 << VMM_SRAM_RD)
    );
}


// ******************************** //
// Register: VMM_READY_SRAM_DOUT    //
// ******************************** //

bool vmm_read_hold_data_ready() {
    return bit_read(
        (volatile uint32_t *)VMM_READY_SRAM_DOUT,
        (1 << VMM_HOLD_DATA_READY)
    );
}

bool vmm_read_adc_data_ready() {
    return bit_read(
        (volatile uint32_t *)VMM_READY_SRAM_DOUT,
        (1 << VMM_ADC_DATA_READY)
    );
}

uint32_t vmm_read_sram_dout() {
    return uint_read(
        (volatile uint32_t *)VMM_READY_SRAM_DOUT,
        (((1 << VMM_SRAM_DOUT_WIDTH) - 1) << VMM_SRAM_DOUT),
        VMM_SRAM_DOUT
    );
}


// ******************************** //
// Register: OUTPUT_ADC             //
// ******************************** //

uint8_t vmm_read_adc(unsigned index) {
    return gpi_read((volatile uint32_t *)(OUTPUT_ADC + (long)index * 4));
}


/* ************************************************************************************* */
/* Input / output formatting                                                             */
/* ************************************************************************************* */

uint8_t vmm_float2bin_vec_in(float val) {
    // 1+6 bits
    // const int bit_count = 6;
    const int max_bin = 63; //(int)pow(2, bit_count) - 1;
    const float max_float = 0.984375; //(float)max_bin / (max_bin +1);

    if (val > max_float) {
        val = max_float;
    } else if (val < -max_float) {
        val = -max_float;
    }

    val *= (float)(max_bin + 1) / max_bin;

    uint8_t ret = (uint8_t)lroundf(fabs(val) * max_bin);

    if (val < 0) {
        ret |= (1 << 6);
    }
    return ret;
}

uint8_t vmm_float2bin_mat_in(float val) {
    // TODO: The same constants are defined in other functions as well. Should
    // we auto-generate a header file including these constant definitions?
    // The same configuration files for these constants could also be used in
    // the python side

    // 2+6bits
    // const int bit_count = 6;
    const int max_bin = 63; //(int)pow(2, bit_count) - 1;
    const float max_float = 0.984375; //(float)max_bin / (max_bin +1);
 
    if (val > max_float) {
        val = max_float;
    } else if (val < -max_float) {
        val = -max_float;
    }

    val *= (float)(max_bin + 1) / max_bin;

    uint8_t ret = (uint8_t)lroundf(fabs(val) * max_bin);

    if (val >= 0) {
        ret |= (1 << 6);
    } else {
        ret |= (1 << 7);
    }
    return ret;
}

float vmm_bin2float_vec_out(uint8_t val) {
    // const int bit_count = 6;
    const int max_bin = 63; //(int)pow(2, bit_count) - 1;
    const float max_float = 0.984375 * VMM_MAT_IN_ROWS; //(float)max_bin / (max_bin +1) * VMM_MAT_IN_ROWS;

    return (val - (max_bin / 2.f)) * (max_float / (max_bin / 2.f));
}

void vmm_print_result() {
    vmm_write_sram_rd(1);
    printf("Result:\n");
    uint8_t result;
    float result_f;
    for (int i = 0; i < VMM_VEC_OUT_DIM; i++) {
        result = (uint8_t)gpi_read((volatile uint32_t *)(OUTPUT_ADC + (long)i * 4));
        printf("%02d: ", i);
        // printf("0x%02x", result);
        result_f = vmm_bin2float_vec_out(result);
        print_float(result_f);
        printf("\n");
    }
}

#endif


