#include <stdio.h>
#include "print_float.h"


void print_float(float f) {
    if (f < 0) {
        printf("-");
        f = -f;
    }
    printf("%d", (int)f);
    printf(".");
    for (int i = 0; i < PRINT_FLOAT_DECIMAL_COUNT; i++) {
        f = (f - (int)f) * 10;
        printf("%d", (int)f );
    }
}


