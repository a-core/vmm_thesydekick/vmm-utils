This repository hosts the VMM (Vector-Matrix Multiplier) utility library originally developed my Mikael Mieskonen.

## README

The vmm-utils library provides functions for using Vector-Matrix Multiplier (VMM) with A-Core.
VMM is a hardware accelerator, used for accelerating vector-matrix multiplication. It takes an input
vector with dimensions 1x36, input matrix with dimensions 36x32, and gives out an output vector with
dimensions 1x32.

A-Core SoC includes VMM on the same chip with the A-Core processor itself. In essence, you can interact
with VMM by writing to certain memory locations on A-Core. Using this library will take care of writing
everything that is needed to correct memory addresses and abstracts away the hardware specific details.

The aim of this library is to make development utilizing VMM with A-Core easier and more portable.
When new versions of A-Core+VMM come out, there might be some changes in the hardware addresses for example.
The idea is that software using this library would not need to be changed, only some parts of the library
implementation itself should be changed.


### Conventions

* All of the function names provided by the vmm-utils library start with `vmm_` prefix since C doesn't
  support namespaces.
* The VMM includes several control bits that can be written or read from. The library includes functions
  starting with `vmm_write_` and `vmm_read_` for writing and reading such control bits. For example if
  a control bit exists with a name `r_tune`, then you can find functions `vmm_write_r_tune` and
  `vmm_read_r_tune` from the library.


### Getting Started

In order to calculate a vector-matrix product, we need to first specify each of the inputs (consisting
of input vector and input matrix elements). After this we can read the outputs (consisting of output
vector elements). Currently the library provides two ways to do this. We can either use binary format
used by the VMM, or just give the inputs as floating point. This example shows how to use the floating
point version. The provided functions are:

* `vmm_write_mat_in_float(unsigned row, unsigned col, float val)`
* `vmm_write_vec_in_float(unsigned col, float val)`
* `vmm_read_vec_out_float(unsigned col)`

These functions provide a way to write each of the input elements one by one. After that each of the output
elements can be read one by one. There are fixed limits for input and output dimensions, but you can also
use the VMM to calculate vector-matrix product for smaller input vector and matrix if you first initialize
all the unused input elements to zeros. If we assume that all input elements have already been initialized
to zero, here is how we could generate a 3x3 identity matrix and a 1x3 input vector with elements 0, 0.5 and
1, and calculate vector matrix product for this:

```c
#include <stdio.h>
#include "vmm_utils.h"
#include "a-core-utils.h"
#include "print_float.h"


void main() {
    printf("\nSTART THE TEST\n\n");
    // NOTE: We are only using a little part of VMM (3x3 matrix instead of 36x32 matrix).
    // For physical hardware we would need to first write all input matrix elements to
    // zero, so we can use smaller matrices. However, in simulation the input matrix is
    // all zeros in the beginning by default. I have not written all of the weights to
    // zeros here because it will take a long time to simulate.

    float mat_in[3][3] = {
        { 1.0, 0.0, 0.0 },
        { 0.0, 1.0, 0.0 },
        { 0.0, 0.0, 1.0 },
    };
    // Write matrix inputs
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            vmm_write_mat_in_float(i, j, mat_in[i][j]);
        }
    }

    float vec_in[] = { 0.0, 0.5, 1.0 };
    // Write vector inputs
    for (int i = 0; i < 3; i++) {
        vmm_write_vec_in_float(i, vec_in[i]);
    }

    float result[3];
    // Read the result and save it into an array
    for (int i = 0; i < 3; i++) {
        result[i] = vmm_read_vec_out_float(i);
    }

    // NOTE: VMM will give approximate results, not exact ones.
    // NOTE: The `print_float()` function is a temporary fix for debugging purposes,
    //       since A-Core can't print floating point numbers normally at the moment.
    // Print the results
    print_float(result[0]);
    printf("\n");
    print_float(result[1]);
    printf("\n");
    print_float(result[2]);
    printf("\n");

    printf("\nEND THE TEST\n\n");
    test_pass();
    test_end();
}


```

**NOTE: At the moment the floating point API in this library assumes that all input vector, input matrix
and output vector elements are between -1 and 1. Anything more than 1 will be rounded to 1 and anything
below -1 will be rounded to -1. This behavior could be altered in the hardware changing the VMM's reference
voltages. The plan is to include a header file at some point where you can specify the used reference
voltages and in this way change the possible floating point input and output ranges.**


### Block Diagram

This is a block diagram showing how A-Core and VMM are physically connected connected in the A-Core SoC.
When using the provided library, you shouldn't need to worry about this part. This is just here to give
a better understanding of how the library internally works, and should be useful for any possible future
developers of this library.

![](doc/figures/figure.svg)

You can write C code to be run on A-Core. From C, you can read and write to AXI4L registers. All the
interaction with VMM is done through reading and writing to these AXI4L registers.

The VMM will perform the vector-matrix multiplication on hardware. This means that in order to calculate
vector-matrix multiplication utilizing the VMM, you only need to specify the the input values and read
the result from memory. The input consist of input vector and input matrix elements. The output consists
of output vector elements.

The VMM needs inputs to be placed in correct addresses in the VMM's SRAM memory. However, this memory is
not directly writable from A-Core. The procedure is to write input data and the corresponding SRAM address
to a specific AXI4L register, from where it will be written to VMM's SRAM memory. The transfer from AXI4L
register to SRAM will happen on a rising clock cycle if we set SRAM write enable bit on from a specific
AXI4L register.

Using for example the function provided by this library `vmm_write_mat_in_bin(row, col, val)` will
1. Write `val` to the correct place in the AXI4L registers
2. Use `row` and `col` to determine which address in VMM's SRAM memory we want to use
3. Write the determined address to correct place in AXI4L registers
4. Set write enable for VMM's SRAM registers. This means that `val` will be written to VMM's SRAM
   address determined with `row` and `col` on the next rising edge of the clock.

In effect, this whole thing specifies one element from input matrix. After setting each of the elements
from input vector and input matrix in a similar manner, you can read output from AXI4L registers using
the functions provided with the library.


### Notes / TODO

* Note that tapeout1 version of A-Core doesn't include a properly working floating point unit. This means that
  floating point calculations have to be done using soft float, which can be very inefficient. We might need to
  implement some kind of fixed point implementation? We also need to implement the effect of VMM reference
  voltages on floating point input and output ranges as mentioned above. The problem is that these reference
  voltages are also used in VMM python model and possibly in other places. Ideally we would read the VMM
  reference voltages from some common configuration file which can be read from different places.
* Regarding floating point operations, also note that printing floating points with printf function is not yet
  implemented on A-Core. The vmm-utils includes a temporary debugging function `print_float(float val)` which
  can be used by including `#include "print_float.h"`. However, note that this function will be removed in later
  version when printing with printf gets implemented. Also note that if simulating with cocotb, you need to print
  newline in order to flush stdout. Just using `print_float(1.0)` for example might not show anything in the
  simulation. You would also need to call `printf("\n")` after this.
* Note that we only have 7 bit input and 6 bit output for the VMM, so all the results are approximate.
* Note that the implementation is wrapped inside `#ifdef ACORE` so you need to have `ACORE` defined in order to
  use the library. This feature might or might not be removed in the future.

More information about the following topics should be added to this README:

* More specific information about the binary format used by VMM
* Detailed information about what each function provided by this library does and doesn't do
* How to set up the control bits correctly (some kind of vmm_init function might be added in the future)
* Discuss the possibility of VMM overflows and their relation to VMM reference voltages


